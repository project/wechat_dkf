<?php
/**
 * @file
 * wechat_dkf.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function wechat_dkf_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'wechat_kf-wechat_kf-field_kf_headimg'
  $field_instances['wechat_kf-wechat_kf-field_kf_headimg'] = array(
    'bundle' => 'wechat_kf',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'wechat_kf',
    'field_name' => 'field_kf_headimg',
    'label' => 'kf_headimg',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'kf_headimg',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'wechat_response_message-transfer_customer_service-field_kfaccount'
  $field_instances['wechat_response_message-transfer_customer_service-field_kfaccount'] = array(
    'bundle' => 'transfer_customer_service',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'wechat_response_message',
    'field_name' => 'field_kfaccount',
    'label' => 'kfaccount',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('kf_headimg');
  t('kfaccount');

  return $field_instances;
}
