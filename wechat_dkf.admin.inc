<?php

/**
 * @file
 * wechat admin pages.
 */
/**
 * Returns a breadcrumb submission form.
 */
function wechat_dkf_wechatkf_callback() {
  $output = "客服列表";
  $output .= '<ul class="action-links"><li>' .l('添加客服','admin/wechat/config/kf/add') . '</li></ul>';
  $kf_list = wechat_dkf_get_kflist();
  //print debug($kf_list);
  if (!empty($kf_list)) {
    $output .= '<table>';
	$output .= '<tr><td>账号</td><td>头像</td><td>ID</td><td>昵称</td></tr>';
	foreach($kf_list['kf_list'] as $kf){
	  $kf_account = $kf["kf_account"];
	  $wechat_kf = wechat_dkf_wechatkf_load_by_account($kf_account);
	  
	  if(empty($wechat_kf)){
	    $wechat_kf = entity_create('wechat_kf', array('type' => 'wechat_kf'));
		$wechat_kf->kf_account = $kf["kf_account"];
		$wechat_kf->nickname = $kf["kf_nick"];
		$wechat_kf->kf_id = '';
        if(!empty($kf["kf_headimgurl"])){
		 // $fid = wechat_dkf_save_remote_image($kf["kf_headimgurl"], $kf["kf_id"]);
		 // $wechat_kf->field_kf_headimg['und'][0]['fid'] = $fid;
		}		
		wechat_dkf_wechatkf_save($wechat_kf);
	  }
	  $output .= '<tr>';
	  $output .= '<td>'. $kf["kf_account"].'</td>';
	  $output .= '<td><img src="'. $kf["kf_headimgurl"].'" width="50px" height="50px"/></td>';
	  $output .= '<td>'. $kf["kf_id"].'</td>';
	  $output .= '<td>'. $kf["kf_nick"].'</td>';
	  $output .= '</tr>';
	}
	$output .= '</table>';
  }

  return $output;
}
function wechat_dkf_save_remote_image($kf_headimgurl, $imagename){
	  //watchdog('wechat', 'Recognition123:' . $xml_obj->Recognition);
    $result = drupal_http_request($kf_headimgurl);	
    if ($result->code == 200) {	
      $dir_uri = file_stream_wrapper_get_instance_by_uri('public://');
      $realpath = $dir_uri->realpath();
	  $kf_headimg_dir = 'public://kf_headimg';
	  $return = file_prepare_directory($kf_headimg_dir);
      if (empty($return)) {
        drupal_mkdir('public://kf_headimg', 0777, TRUE);
      } 
      $filename = $realpath . "/kf_headimg/". $imagename . "." . 'jpg';
      file_put_contents($filename, $result->data);

      $uri = 'public://kf_headimg/'. $imagename . "." . 'jpg';
      $file = new StdClass;
      $file->uid = 1;
      $file->filename = basename($uri);
      $file->uri = $uri;
      $file->filemime = file_get_mimetype($uri);
      // This is gagged because some uris will not support it.
      $file->filesize = @filesize($uri);
      $file->timestamp = REQUEST_TIME;
      $file->status = FILE_STATUS_PERMANENT;
      $file->is_new = TRUE;
      $file = file_save($file);
	  return $file->fid;
  }
  else{
    return false;
  }
	  //watchdog('wechat', 'file:' . $file->fid);

}

function wechat_dkf_wechatkf_onlinelist_callback() {
  $output = "客服在线列表";
  //$output .= '<ul class="action-links"><li>' .l('添加客服','admin/wechat/config/kf/add') . '</li></ul>';
  $kf_list = wechat_dkf_get_online_kflist();
  //drupal_set_message(var_export($kf_list,true));
  //print debug($kf_list);
  if (!empty($kf_list)) {
    $output .= '<table>';
	$output .= '<tr><td>账号</td><td>状态</td><td>ID</td><td>自动最大接入数</td><td>接待的会话数</td></tr>';
	foreach($kf_list['kf_online_list'] as $kf){
	/*
	  $kf_account = $kf["kf_account"];
	  $wechat_kf = wechat_dkf_wechatkf_load_by_account($kf_account);
	  if(empty($wechat_kf)){
	    $wechat_kf = entity_create('wechat_kf', array('type' => 'wechat_kf'));
		$wechat_kf->kf_account = $kf["kf_account"];
		$wechat_kf->nickname = $kf["kf_nick"];
		$wechat_kf->kf_id = '';
        if(!empty($kf["kf_headimgurl"])){
		  $fid = wechat_dkf_save_remote_image($kf["kf_headimgurl"], $kf["kf_id"]);
		  $wechat_kf->field_kf_headimg['und'][0]['fid'] = $fid;
		}		
		wechat_dkf_wechatkf_save($wechat_kf);
	  }
	  */
	  $output .= '<tr>';
	  $output .= '<td>'. $kf["kf_account"].'</td>';
	  $output .= '<td>'. $kf["status"].'</td>';
	  $output .= '<td>'. $kf["kf_id"].'</td>';
	  $output .= '<td>'. $kf["auto_accept"].'</td>';
	  $output .= '<td>'. $kf["accepted_case"].'</td>';
	  $output .= '</tr>';
	}
	$output .= '</table>';
  }

  return $output;
}

function wechat_dkf_wechatkf_sync_callback(){
  $output = "";
  $output .= '<div>' . "点击下面的同步按钮，将会把微信里面的客服信息，同步到本地" . '</div>';
  $wechat_dkf_wechatkf_sync_form = drupal_get_form('wechat_dkf_wechatkf_sync_form');
  $output .= drupal_render($wechat_dkf_wechatkf_sync_form);
  return $output;
}

function wechat_dkf_wechatkf_sync_form($form, &$form_state){
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('同步'),
  );
  return $form;
}

function wechat_dkf_wechatkf_sync_form_submit(&$form, &$form_state) {
  $kf_list = wechat_dkf_get_kflist();
  //print debug($kf_list);
  if (!empty($kf_list)) {
	foreach($kf_list['kf_list'] as $kf){
	  $kf_account = $kf["kf_account"];
	  $wechat_kf = wechat_dkf_wechatkf_load_by_account($kf_account);
	  if(empty($wechat_kf)){
	    $wechat_kf = entity_create('wechat_kf', array('type' => 'wechat_kf'));
		$wechat_kf->kf_account = $kf["kf_account"];
		$wechat_kf->nickname = $kf["kf_nick"];
		$wechat_kf->kf_id = '';
        if(!empty($kf["kf_headimgurl"])){
		  //$fid = wechat_dkf_save_remote_image($kf["kf_headimgurl"], $kf["kf_id"]);
		  //$wechat_kf->field_kf_headimg['und'][0]['fid'] = $fid;
		}		
		wechat_dkf_wechatkf_save($wechat_kf);
	  }

	}
    drupal_set_message('客服信息同步成功');
  }
  
}
/**
 * Returns a breadcrumb submission form.
 */
function wechat_dkf_wechatkf_add() {
  $wechat_kf = entity_create('wechat_kf', array('type' => 'wechat_kf'));;

  $output = drupal_get_form('wechat_dkf_wechatkf_edit_form', $wechat_kf);

  return $output;
}

/**
 * Returns a breadcrumb submission form.
 */
function wechat_dkf_wechatkf_edit() {
  $id = arg(5);
  //$wechat_kf = entity_create('wechat_kf', array('type' => 'wechat_kf'));;
  $wechat_kf = wechat_dkf_wechatkf_load($id);
  $output = drupal_get_form('wechat_dkf_wechatkf_edit_form', $wechat_kf);

  return $output;
}
/**
 * Form callback: create or edit a wechat_response_message.
 *
 * @param $wechat_kf
 *   The wechat_kf object to edit.
 */
function wechat_dkf_wechatkf_edit_form($form, &$form_state, $wechat_kf) {
  // Add the default field elements.
  $form['kf_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Kf account'),
    '#default_value' => isset($wechat_kf->kf_account) ? $wechat_kf->kf_account : '',
	'#disabled' => empty($wechat_kf->kf_account) ? FALSE : TRUE,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  
  $form['nickname'] = array(
    '#type' => 'textfield',
    '#title' => t('Nickname'),
    '#default_value' => isset($wechat_kf->nickname) ? $wechat_kf->nickname : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  
  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => isset($wechat_kf->password) ? $wechat_kf->password : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );  
  // Add the field related form elements.
  $form_state['wechat_kf'] = $wechat_kf;
  field_attach_form('wechat_kf', $wechat_kf, $form, $form_state);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );
  if (!empty($wechat_kf->id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete wechat kf'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('wechat_dkf_wechatkf_edit_form_submit_delete')
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'wechat_dkf_wechatkf_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the model form
 */
function wechat_dkf_wechatkf_edit_form_validate(&$form, &$form_state) {
  $wechat_kf = $form_state['wechat_kf'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('wechat_kf', $wechat_kf, $form, $form_state);
}


/**
 * Form API submit callback for the model form.
 * 
 * @todo remove hard-coded link
 */
function wechat_dkf_wechatkf_edit_form_submit(&$form, &$form_state) {
  
  $wechat_kf = &$form_state['wechat_kf'];
  $wechat_kf->kf_account = $form_state['values']['kf_account'];
  $wechat_kf->nickname = $form_state['values']['nickname'];
  $wechat_kf->password = $form_state['values']['password'];
  $wechat_kf->kf_id = "";
  // Notify field widgets.
  field_attach_submit('wechat_kf', $wechat_kf, $form, $form_state);
  //print debug($wechat_kf);
 
  $ret = null;
  if (!empty($wechat_kf->id)) {
    $ret = wechat_dkf_update_kf_account($wechat_kf);
  }
  else {
    $ret = wechat_dkf_add_kf_account($wechat_kf);
  }
  // Save the wechat_kf
  if (!empty($ret)) {
    //drupal_set_message(var_export($wechat_kf, true));
    $field_kf_headimg_fid = isset($wechat_kf->field_kf_headimg['und'][0]['fid']) ? $wechat_kf->field_kf_headimg['und'][0]['fid'] : "";
	//drupal_set_message("field_kf_headimg_fid:" .$field_kf_headimg_fid);
	if(!empty($field_kf_headimg_fid)){
      $file = file_load($field_kf_headimg_fid);
      $uri = $file->uri;
      $realpath = drupal_realpath($uri);
      $we_obj = wechat_init_obj_with_access_token();
	  //drupal_set_message("realpath:" .$realpath);
      $return = $we_obj->setKFHeadImg($wechat_kf->kf_account, $realpath);	  
	}

    wechat_dkf_wechatkf_save($wechat_kf);
    $form_state['redirect'] = 'admin/wechat/config/kf';
  }
  else{
    $we_obj = wechat_init_obj_with_access_token();
	drupal_set_message('添加/更新客服失败,错误代码:' . $we_obj->errCode );
    //drupal_set_message('添加/更新客服失败');
  }
}

function wechat_dkf_wechatkf_edit_form_submit_delete(&$form, &$form_state) {
  
  $wechat_kf = &$form_state['wechat_kf'];
  wechat_dkf_wechatkf_delete($wechat_kf);
  wechat_dkf_delete_kf_account($wechat_kf);

  $form_state['redirect'] = 'admin/wechat/config/kf';
}

