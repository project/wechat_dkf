<?php
/**
 * @file
 * Provide default_views for wechat.
 */

/**
 * Implements hook_views_default_views().
 */
function wechat_dkf_views_default_views() {
$view = new view();
$view->name = 'dkf';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'wechat_kf';
$view->human_name = 'dkf';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'dkf';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = '更多';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = '应用';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = '重设';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = '排序标准';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = '每页条目数';
$handler->display->display_options['pager']['options']['expose']['offset_label'] = '偏移量';
$handler->display->display_options['pager']['options']['tags']['first'] = '« 第一页';
$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ 前一页';
$handler->display->display_options['pager']['options']['tags']['next'] = '下一页 ›';
$handler->display->display_options['pager']['options']['tags']['last'] = '末页 »';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_kf_headimg' => 'field_kf_headimg',
  'kf_account' => 'kf_account',
  'nickname' => 'nickname',
  'password' => 'password',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_kf_headimg' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'kf_account' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nickname' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'password' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* 字段: Wechat kf: Wechat Kf ID */
$handler->display->display_options['fields']['id']['id'] = 'id';
$handler->display->display_options['fields']['id']['table'] = 'wechat_kf';
$handler->display->display_options['fields']['id']['field'] = 'id';
$handler->display->display_options['fields']['id']['label'] = '';
$handler->display->display_options['fields']['id']['exclude'] = TRUE;
$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['id']['separator'] = '';
/* 字段: Wechat kf: kf_headimg */
$handler->display->display_options['fields']['field_kf_headimg']['id'] = 'field_kf_headimg';
$handler->display->display_options['fields']['field_kf_headimg']['table'] = 'field_data_field_kf_headimg';
$handler->display->display_options['fields']['field_kf_headimg']['field'] = 'field_kf_headimg';
$handler->display->display_options['fields']['field_kf_headimg']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_kf_headimg']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* 字段: Wechat kf: kf_account */
$handler->display->display_options['fields']['kf_account']['id'] = 'kf_account';
$handler->display->display_options['fields']['kf_account']['table'] = 'wechat_kf';
$handler->display->display_options['fields']['kf_account']['field'] = 'kf_account';
/* 字段: Wechat kf: nickname */
$handler->display->display_options['fields']['nickname']['id'] = 'nickname';
$handler->display->display_options['fields']['nickname']['table'] = 'wechat_kf';
$handler->display->display_options['fields']['nickname']['field'] = 'nickname';
/* 字段: Wechat kf: password */
$handler->display->display_options['fields']['password']['id'] = 'password';
$handler->display->display_options['fields']['password']['table'] = 'wechat_kf';
$handler->display->display_options['fields']['password']['field'] = 'password';
/* 字段: 全局: 自定义文本 */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '编辑';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['path'] = 'admin/wechat/config/kf/edit/[id]';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/wechat/config/kf';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = '多客服';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['dkf'] = array(
  t('Master'),
  t('dkf'),
  t('更多'),
  t('应用'),
  t('重设'),
  t('排序标准'),
  t('Asc'),
  t('Desc'),
  t('每页条目数'),
  t('- All -'),
  t('偏移量'),
  t('« 第一页'),
  t('‹ 前一页'),
  t('下一页 ›'),
  t('末页 »'),
  t('.'),
  t('kf_headimg'),
  t('kf_account'),
  t('nickname'),
  t('password'),
  t('自定义文本'),
  t('编辑'),
  t('Page'),
);

  $views[$view->name] = $view; 
  return $views;
}